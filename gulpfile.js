/**
 * Created by alexander on 03.03.15.
 */
'use strict';

var gulp = require('gulp'),
	watch = require('gulp-watch'), // нужен для наблюдения за изменениями файлов (со встроенным были проблемы)
    prefixer = require('gulp-autoprefixer'), // автоматически добавляет вендорные префиксы к CSS свойствам
	uglify = require('gulp-uglify'), // будет сжимать наш JS
	cssmin = require('gulp-minify-css'), // нужен для сжатия CSS кода
	jade = require('gulp-jade'), // для компиляции нашего JADE кода (можно использовать HTML и собирать rigger'ом)
	sass = require('gulp-sass'), // для компиляции нашего SASS кода (можно использовать LESS)
	sourcemaps = require('gulp-sourcemaps'), // возьмем для генерации css sourscemaps, которые будут помогать нам при отладке кода
	rigger = require('gulp-rigger'), // позволяет импортировать один файл в другой простой конструкцией (//= footer.html)
	imagemin = require('gulp-imagemin'), // для сжатия картинок
	spritesmith  = require('gulp.spritesmith'), // для создания спрайта
    urlAdjuster = require('gulp-css-url-adjuster'), // смена адреса
	pngquant = require('imagemin-pngquant'), // дополнения к gulp-imagemin, для работы с PNG
	rimraf = require('rimraf'), // rm -rf для ноды
	browserSync = require("browser-sync"), // с помощью этого плагина мы можем легко развернуть локальный dev сервер с блэкджеком и livereload
	reload = browserSync.reload,
	styleguide = require('devbridge-styleguide');

var path = {
	build: { //Тут мы укажем куда складывать готовые после сборки файлы
		html: 'build/',
		js: 'build/js/',
		css: 'build/css/',
		img: 'build/img/',
		fonts: 'build/fonts/'
	},	
	src: { //Пути откуда брать исходники
		html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
		jade: 'src/*.jade', //Синтаксис src/*.jade говорит gulp что мы хотим взять все файлы с расширением .jade
		js: 'src/js/main.js', //В стилях и скриптах нам понадобятся только main файлы
		style: 'src/style/main.scss',
		img: 'src/img/**/*.*',
		sprite: 'src/sprite/**/*.*',
		spriteRetina: 'src/sprite/',
		scssSprite: 'src/style/partials/',
		fonts: 'src/fonts/**/*.*'
	},
	watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
		html: 'src/**/*.html',
		jade: 'src/**/*.jade',
		js: 'src/js/**/*.js',
		style: 'src/style/**/*.scss',
		sprite: 'src/sprite/**/*.*',
		img: 'src/img/**/*.*',
		fonts: 'src/fonts/**/*.*'
	},
	clean: './build'
};

var config = {
	server: {
		baseDir: "./"
	},
	open: false,
	tunnel: false,
	host: 'localhost',
	port: 9001,
	logPrefix: "frontend_dev",
	notify: false
};

// var config = {
// 	port: 9001, // Set the server port. Defaults to 8080. 
// 	host: "localhost", // Set the address to bind to. Defaults to 0.0.0.0 or process.env.IP. 
// 	root: "./build", // Set root directory that's being server. Defaults to cwd. 
// 	open: false, // When false, it won't load your browser by default. 
// 	watch: "./src",
// 	ignore: 'scss,my/templates', // comma-separated string for paths to ignore 
// 	file: "index.html", // When set, serve this file for every 404 (useful for single-page applications) 
// 	wait: 1000, // Waits for all changes, before reloading. Defaults to 0 sec. 
// 	//mount: [['/components', './node_modules']], // Mount a directory to a route. 
// 	logLevel: 2 // 0 = errors only, 1 = some, 2 = lots 
// };


gulp.task('html:build', function(){
	gulp.src(path.src.jade) //Выберем файлы по нужному пути
		//.pipe(rigger()) //Прогоним через rigger
		.pipe(jade({pretty: '	'}))
		.pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
		.pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function () {
	gulp.src(path.src.js) //Найдем наш main файл
		.pipe(rigger()) //Прогоним через rigger
		.pipe(sourcemaps.init()) //Инициализируем sourcemap
		.pipe(uglify()) //Сожмем наш js
		.pipe(sourcemaps.write()) //Пропишем карты
		.pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
		.pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('style:build', function () {
	gulp.src(path.src.style) //Выберем наш main.scss
		.pipe(sourcemaps.init()) //То же самое что и с js
		.pipe(sass()) //Скомпилируем
		.pipe(prefixer({browsers: ['ie 8', 'ie 9', 'Firefox 5.0', 'Opera 12.0', 'Safari 4']})) //Добавим вендорные префиксы
        .pipe(urlAdjuster({
            replace:  ['../../','../']
        }))
		.pipe(cssmin())
		.pipe(gulp.dest(path.build.css)) //И в build
		// bitrix
		/*.pipe(urlAdjuster({
			replace:  ['../','']
		}))
		.pipe(rename({
			basename: "template_styles"
		}))
		.pipe(cssmin())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.project.template)) //И в project
		*/
		.pipe(reload({stream: true}));
});

// создание спрайта
gulp.task('sprite:build', function() {
	var spriteData =
		gulp.src(path.src.sprite)
			.pipe(spritesmith({
				imgName: 'sprite.png',				
				cssName: '_sprite.scss',
				cssFormat: 'scss',
				algorithm: 'top-down', // binary-tree - плохой алгоритм для ретины 
				imgPath: '../img/sprite.png',
				padding: 5,
				// retina	
				//retinaSrcFilter: [path.src.spriteRetina + '*-2x.png'],
				//retinaImgName: 'sprite-2x.png',
				//retinaImgPath: '../img/sprite-2x.png',
				cssVarMap: function(sprite) {
					sprite.name = 'ic-' + sprite.name
				}
			}));

	spriteData.img.pipe(gulp.dest(path.build.img)); // путь, куда сохраняем картинку
	spriteData.css.pipe(gulp.dest(path.src.scssSprite)); // путь, куда сохраняем стили
	spriteData.pipe(reload({stream: true}));
});

// копирование и сжатие картинок
gulp.task('image:build', function () {
    gulp.src(path.src.img) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});

// копирование шрифтов
gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
    'html:build',
    'js:build',
	'sprite:build',
	'style:build',
	'fonts:build',
    'image:build'
]);

gulp.task('watch', function(){
	watch([path.watch.jade], function(event, cb) {
		gulp.start('html:build');
	});
	watch([path.watch.style], function(event, cb) {
		gulp.start('style:build');
	});
	watch([path.watch.js], function(event, cb) {
		gulp.start('js:build');
	});
	watch([path.watch.sprite], function(event, cb) {
		gulp.start('sprite:build');
	});
	watch([path.watch.img], function(event, cb) {
		gulp.start('image:build');
	});
	watch([path.watch.fonts], function(event, cb) {
		gulp.start('fonts:build');
	});
});

// запуск styleguide
gulp.task('start-styleguide', function () {
	styleguide.startServer({
    	styleguidePath: './styleguide/'
	});
});

// запуск сервера на прослушку
gulp.task('webserver', function () {
	browserSync(config);
});

// Очистка папки build
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

// Запуск всей сборки
gulp.task('default', ['build', 'webserver', 'watch', 'start-styleguide']);