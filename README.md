#Frontend Kit
###JADE(html) + SCSS(css) + JS
##Installing:

``` console
    npm i
    bower i    
```

##Run

``` console
    gulp sprite:build
    gulp default
```

##Dependencies:

* git
* node, npm
* bower

##For retina sprite uncommented line in
* task for spire in gulpfile.js
* rule in src/style/patials/_mixins.scss
